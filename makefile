include config.env
export $(shell sed 's/=.*//' config.env)

scan:
	docker run -it --name sist2-scan \
	    -v $LOTUS_FILE_ROOT:/files \
	    -v $LOTUS_SCRATCH:/scratch \
	    simon987/sist2 scan -t 4 /files -o /scratch/file_index
.PHONY: scan

index:
	docker run -it --network host --name sist2-index \
	    -v $LOTUS_SCRATCH:/scratch \
	    simon987/sist2 index /scratch/file_index
.PHONY: index

web:
	docker run --rm --network host -d --name sist2-web \
	    -v $LOTUS_SCRATCH:/scratch \
	    -v $LOTUS_FILE_ROOT:/files
	    simon987/sist2 web --bind 0.0.0.0 /scratch/file_index
.PHONY: web

start:
	docker-compose up -d

.PHONY: start
